/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.delivery;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DeliveryFields {

    ADDRESS("//input[@name='address']"),
    POST_CODE("//input[@name='postcode']"),
    TOWN("//input[@name='town']"),
    NAME("//input[@name='surname']"),
    EMAIL("//input[@name='email']"),
    PHONE("//input[@name='phonenumber']"),
    COMPANY("//input[@name='companyname']"),
    DELIVERY_TIME("//select[@name='deliverytime']"),
    REMARKS("//textarea[@name='remarks']"),
    SAVE_REMARKS("//input[@name='saveremarks']"),
    PAY_WITH("//select[@name='payswith']");

    @Getter
    private final String xpath;
}
