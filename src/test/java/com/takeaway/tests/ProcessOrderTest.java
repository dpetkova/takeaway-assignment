package com.takeaway.tests;

import com.opencsv.exceptions.CsvException;
import com.takeaway.pages.delivery.DeliveryPage;
import com.takeaway.pages.modules.SearchWidget;
import com.takeaway.pages.modules.ShoppingBasket;
import com.takeaway.pages.ordered.OrderedPage;
import com.takeaway.pages.restaurant.RestaurantListPage;
import com.takeaway.pages.restaurant.RestaurantPage;
import com.takeaway.utils.CsvReader;
import com.takeaway.utils.TestUtil;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.takeaway.pages.delivery.DeliveryFields.ADDRESS;
import static com.takeaway.pages.delivery.DeliveryFields.EMAIL;
import static com.takeaway.pages.delivery.DeliveryFields.NAME;
import static com.takeaway.pages.delivery.DeliveryFields.PHONE;
import static com.takeaway.pages.delivery.DeliveryFields.POST_CODE;
import static com.takeaway.pages.delivery.DeliveryFields.TOWN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

@Slf4j
public class ProcessOrderTest extends TestUtil {

    @DataProvider(name = "process-order-data")
    public static Object[][] dataProviderNameFromFile() throws IOException, CsvException {
        return CsvReader.readCsvFile("process-order-data.csv");
    }

    @BeforeMethod(description = "Load application")
    public void loadPage() {
        driver.loadUrl(MAIN_URL);
        driver.maximizeWindow();
    }

    @Test(description = "Smoke test: Process order", priority = 1, dataProvider = "process-order-data")
    public void processOrderSmokeTest(String searchPostCode, String deliveryArea, String restaurantName, String meal,
                                      String street, String postCode, String town, String name, String phone,
                                      String email, String payWithSize, String expectedMessage) {

        SearchWidget searchWidget = new SearchWidget(driver);
        searchWidget.loadRestaurantsByArea(searchPostCode, deliveryArea);

        RestaurantListPage restaurantListPage = new RestaurantListPage(driver);
        restaurantListPage.selectRestaurantByName(restaurantName);

        RestaurantPage restaurantPage = new RestaurantPage(driver);
        restaurantPage.addMealToBasket(meal);

        ShoppingBasket shoppingBasket = new ShoppingBasket(driver);
        shoppingBasket.orderMealAndProceed();

        DeliveryPage deliveryPage = new DeliveryPage(driver);
        deliveryPage.fillDeliveryInfo(ADDRESS, street)
                .fillDeliveryInfo(POST_CODE, postCode)
                .fillDeliveryInfo(TOWN, town)
                .fillDeliveryInfo(NAME, name)
                .fillDeliveryInfo(PHONE, phone)
                .fillDeliveryInfo(EMAIL, email)
                .selectPayWithOption(payWithSize)
                .buy();

        OrderedPage orderedPage = new OrderedPage(driver);
        log.info(orderedPage.getOrderReferenceNumber());
        assertThat(orderedPage.getConfirmationDeliveryTitle(), containsString(expectedMessage));
    }

}
