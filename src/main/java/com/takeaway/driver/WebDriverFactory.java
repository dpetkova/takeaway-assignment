/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.driver;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.utils.Config;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {

    private WebDriverWrapper driver;
    private static final String BROWSER = Config.get("browser");
    private static final String IS_LOCAL = Config.get("webdriver.local.enabled");
    private static final String CHROME_URL = Config.get("chrome.remote.url");

    /**
     * Creates a new WebDriver instance
     * @return instance of the WebDriver
     */
    public WebDriverWrapper getDriver() {
        if (driver == null && BROWSER != null) {
            WebDriver currentDriver;
            switch (BROWSER.toLowerCase()) {
                case "chrome":
                    if (localBrowser()) {
                        currentDriver = new LocalChromeDriver();
                    } else {
                        try {
                            currentDriver = new RemoteChromeDriver(new URL(CHROME_URL));
                        } catch (MalformedURLException e) {
                            throw new RuntimeException(e); //TODO make own
                        }
                    }
                    break;
                case "firefox":
                    currentDriver = new FirefoxDriver();
                    break;
                case "edge":
                    currentDriver = new EdgeDriver();
                    break;
                default:
                    throw new InvalidArgumentException("Unsupported browser: " + BROWSER);
            }

            driver = new WebDriverWrapper(currentDriver);
        }

        return driver;
    }

    private static boolean localBrowser() {
        return "true".equals(IS_LOCAL);
    }

}
