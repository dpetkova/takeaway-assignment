/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.delivery;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class DeliveryPage extends BasePage {

    @FindBy(id = "checkoutform")
    private WebElement checkoutForm;

    @FindBy(xpath = "//div[@class='checkout-orderbutton-btn']//input")
    private WebElement orderAndPayButton;

    public DeliveryPage(WebDriverWrapper driver) {
        super(driver);
        waitForPageLoad(checkoutForm);
    }

    /**
     * Fills information in field of delivery form
     *
     * @param field field to be filled in
     * @param info  data to be populated
     * @return the current page
     */
    public DeliveryPage fillDeliveryInfo(DeliveryFields field, String info) {
        WebElement input = getFormElement(field);
        clearAndFill(input, info);
        return this;
    }

    /**
     * Selects `pay with` value based on a 0-index corresponding to the amount of money e.g. lowest amount will have size=0
     * @param size 0-based index corresponding to the amount of money
     * @return the current page
     */
    public DeliveryPage selectPayWithOption(String size) {
        String info = getPaymentValueBySize(Integer.parseInt(size));
        return fillDeliveryInfo(DeliveryFields.PAY_WITH, info);
    }

    /**
     * Orders delivery
     */
    public void buy() {
        scrollToElement(orderAndPayButton);
        waitAndClick(orderAndPayButton);
    }

    private String getPaymentValueBySize(int size) {
        WebElement input = getFormElement(DeliveryFields.PAY_WITH);
        List<WebElement> allOptions = input.findElements(By.xpath(".//option"));

        List<String> allValues = allOptions.stream()
                .map(option -> option.getAttribute("value"))
                .sorted()
                .collect(Collectors.toList());

        WebElement element;
        if (size == 0) {
            element = input.findElement(By.xpath(".//option[normalize-space(@value)='']"));
        } else {
            element = input.findElement(By.xpath(".//option[@value='" + allValues.get(size) + "']"));
        }

        return element.getText();
    }

    private WebElement getFormElement(DeliveryFields field) {
        return getElementByXpath(field.getXpath());
    }
}
