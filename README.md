# Web Automation Framework

---
## Table of content
* Setup and installation
    * Install Java SE Development Kit 8 or above
    * Install IDE
    * Install Git
    * Install Chrome
    * Install Docker
* Clone repository  
* Start test execution
* Test Report
---
## Setup and installation

1. Install **Java** SE Development Kit 8 or above
    * [Download Java](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
    * [Install Java](https://docs.oracle.com/en/java/javase/11/install/installation-jdk-microsoft-windows-platforms.html)

2. Install **IDE** - IntelliJ IDEA (optional)
    * [Donwload IntelliJ IDEA](https://www.jetbrains.com/idea/download/#section=windows)
    * [Install IntelliJ IDEA](https://www.jetbrains.com/help/idea/installation-guide.html)

3. Install **Git**
    * [Download Git](https://git-scm.com/downloads)
    
      When you've successfully started the installer, you should see the Git Setup wizard screen. Follow the Next and Finish prompts to complete the installation

4. Install **Chrome** Version 90

5. Install **Docker** for Windows (for remote tests execution)
    * [Download and install Docker](https://docs.docker.com/docker-for-windows/install/)

## Configurations

1. Environment variables

| Environment Variable | Value |
| --- | --- |
| JAVA_HOME | Path to JDK (e.g. C:\Program Files\Java\jdk1.8.0_65) |
| PATH | %JAVA_HOME%\bin |

2. Project settings 

Automation framework configuration is located in `src/main/resources/config/config.properties`

| property | default value | comment |
| --- | --- | --- |
| browser | chrome | Browser to run tests on e.g. chrome, firefox, edge |
| headless.enabled | false | Enable browser headless mode |
| headless.user-agent| Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 | User agent to override the browser's agent when headless mode is enabled | 
| webdriver.local.enabled | true | Decide whether to start local or remote browser | 
| webdriver.chrome.driver | src/main/resources/drivers/chromedriver.exe | Path to the local chrome webdriver | 
| webdriver.gecko.driver | src/main/resources/drivers/geckodriver.exe | Path to the local firefox webdriver | 
| webdriver.edge.driver | src/main/resources/drivers/msedgedriver.exe | Path to the local edge webdriver | 
| chrome.remote.url | http://localhost:4445/wd/hub | URL to remote webdriver hub | 
| app.url | https://www.thuisbezorgd.nl/en/ | AUT staring page | 
| wait.element | 15 | Timeout in seconds when an expectation is called | 
| temp.dir | c:/automation/temp/ | Path to the directory where temp data will be stored | 

TestNG configuration is located in `src/test/resources/testng.xml`

| property | default value | comment |
| --- | --- | --- |
| parallel | methods | Mechanism used to determine how to use parallel threads when running tests e.g. methods, tests, classes |
| thread-count | 1 | The default number of threads to use when running tests in parallel. |


## Clone repository

```
git clone https://dpetkova@bitbucket.org/dpetkova/selenium-template.git
```

## Set up remote webdriver execution
Only in case you don't want to execute tests with local driver

1. Set prorperty **webdriver.local.enabled** in `src/main/resources/config/config.properties` to **false**
2. Start Docker Desktop
3. Run the docker pull command for standalone-chrome to download the latest version of the Chrome container image
```
docker pull selenium/standalone-chrome
```
4. Run the container exposed on port number 4445 to connect to the Chrome WebDriver. If you want to change the port you must also change chrome.remote.url property
```
docker run -d -p 4445:4444 -v /dev/shm:/dev/shm selenium/standalone-chrome
```
5. Verify that the container is running
```
docker ps
```

## Start test execution
Open a command window and navigate to project directory that contains `pom.xml` file. 

* To run *all tests* execute command
```
mvnw clean test
```

* To run *specific test* execute command
```
mvnw clean test -Dtest={ClassName}#{TestName}
```

## Test Report
The Surefire Report Plugin parses the generated TEST-*.xml files under **${project-dir}/target/surefire-reports** and creates web interface version of the test results -`index.html`