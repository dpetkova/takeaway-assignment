/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.modules;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchWidget extends BasePage {

    @FindBy(xpath = "//input[contains(@class, 'location-panel-search-input')]")
    protected WebElement locationSearchInput;

    @FindBy(id = "submit_deliveryarea")
    private WebElement showButton;

    @FindBy(xpath = "//div[@class='popupoptions']//a")
    private List<WebElement> availableDeliveryAreas;

    @FindBy(id = "ideliveryareaerror")
    private WebElement deliveryError;

    public SearchWidget(WebDriverWrapper driver) {
        super(driver);
    }

    /**
     * Loads restaurant page for given location and chooses amongst multiple available locations if any
     * @param searchedLocation desired location to search for
     * @param availableArea real location to choose, null when there is no multiple available locations
     */
    public void loadRestaurantsByArea(String searchedLocation, String availableArea) {
        typeLocation(searchedLocation);
        if (availableArea != null) {
            show(true);
            selectDeliveryArea(availableArea);
        } else {
            show(false);
        }
    }

    /**
     * Loads restaurant page for given location
     * @param searchedLocation desired location to search for
     */
    public void loadRestaurantsByArea(String searchedLocation) {
        loadRestaurantsByArea(searchedLocation, null);
    }

    /**
     * Shows available areas by search location and waits for error message
     * @param searchedLocation the search location
     */
    public void loadAreasWithError(String searchedLocation) {
        typeLocation(searchedLocation);
        showAreasWithError();
    }

    /**
     * Returns error message text
     * @return the message
     */
    public String getDeliveryErrorText() {
        return getText(deliveryError);
    }

    /**
     * Returns list of all available delivery areas
     * @return the delivery areas
     */
    public List<String> getAllAvailableDeliveryAreas() {
        return getText(availableDeliveryAreas);
    }

    /**
     * Types in search field
     * @param location the location to search for
     * @return current page
     */
    public SearchWidget typeLocation(String location) {
        clearAndFill(locationSearchInput, location);
        return this;
    }

    /**
     * Clicks on show restaurants button
     * @param expectList true - wait for list with available areas, false - wait for search field to disappear
     */
    public void show(boolean expectList) {
        locationSearchInput.sendKeys(Keys.TAB, Keys.ENTER);
        if (expectList) {
            waitForElementsVisibility(availableDeliveryAreas);
        } else {
            waitForElementToDisappear(locationSearchInput);
        }
    }

    /**
     * Returns the text the input search field
     * @return the search text
     */
    public String getSearchedAreaText() {
        return getValueByJs(locationSearchInput);
    }

    private void showAreasWithError() {
        waitAndClick(showButton);
        waitForElementVisibility(deliveryError);
    }

    private void selectDeliveryArea(String location) {
        WebElement area = getDeliveryArea(location);
        waitAndClick(area);
        waitForElementToDisappear(area);
    }

    private WebElement getDeliveryArea(String location) {
        return getElementFromListByText(availableDeliveryAreas, location);
    }
}
