/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.ordered;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OrderedPage extends BasePage {

    @FindBy(xpath = "//div[@id='scoober-tracker']//h1")
    private WebElement deliveryTitle;

    @FindBy(xpath = "//span[@class='order-purchaseid']")
    private WebElement referenceNumber;

    public OrderedPage(WebDriverWrapper driver) {
        super(driver);
        waitForPageLoad(deliveryTitle);
    }

    /**
     * Returns the confirmation delivery title of an order
     * @return delivery title of an order
     */
    public String getConfirmationDeliveryTitle() {
        return getText(deliveryTitle);
    }

    /**
     * Returns the reference number of an order
     * @return the reference number
     */
    public String getOrderReferenceNumber() {
        return getText(referenceNumber);
    }
}
