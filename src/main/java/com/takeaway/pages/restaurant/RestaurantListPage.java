/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.restaurant;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RestaurantListPage extends BasePage {

    @FindBy(xpath = "//div[contains(@id, 'irestaurantlist')]" +
            "//div[contains(@class, 'js-restaurant') and not(contains(@id, 'SingleRestaurantTemplateIdentifier'))]" +
            "//div[contains(@class, 'detailswrapper')]//h2//a")
    private List<WebElement> allRestaurantsList;

    @FindBy(xpath = "//div[contains(@class,'js-restaurantlist')]")
    private WebElement restaurantsListContainer;

    @FindBy(id = "kitchen-types")
    private WebElement kitchenTypes;

    public RestaurantListPage(WebDriverWrapper driver) {
        super(driver);
    }

    /**
     * Returns the first WebElement containing the given restaurant name
     * @param name restaurant name
     * @return the WebElement
     */
    public WebElement getRestaurantByName(String name) {
        waitForElementVisibility(restaurantsListContainer);
        return getElementFromListByText(allRestaurantsList, name);
    }

    /**
     * Returns list of all restaurants' names
     * @return list of all restaurants' names
     */
    public List<String> getAllRestaurantsNames() {
        waitForElementVisibility(restaurantsListContainer);
        return getText(allRestaurantsList);
    }

    /**
     * Selects and opens the page of a given restaurant
     * @param name restaurant name
     */
    public void selectRestaurantByName(String name) {
        WebElement restaurant = getRestaurantByName(name);
        scrollBelowElement(restaurant, kitchenTypes);
        waitAndClick(restaurant);
        waitForElementToDisappear(restaurant);
    }
}
