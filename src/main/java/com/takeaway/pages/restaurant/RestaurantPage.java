/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.restaurant;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.pages.BasePage;
import com.takeaway.pages.modules.ShoppingBasket;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RestaurantPage extends BasePage {

    protected ShoppingBasket shoppingBasket;

    @FindAll({
            @FindBy(xpath = "//section[@data-qa='page-section']//h1//font"),
            @FindBy(xpath = "//div[@class='restaurant-name']"),
            @FindBy(xpath = "//div[@class='menucard-container']")
    })
    private WebElement restaurantName;

    @FindAll({
            @FindBy(id = "category-nav"),
            @FindBy(xpath = "//div[@class='js-menu-category-bar-sticky']")
    })
    private WebElement categoryNavigation;

    @FindBy(xpath = "//div[contains(@class,'menucard__category-name')]")
    private List<WebElement> menuCardCategoryName;

    public RestaurantPage(WebDriverWrapper driver) {
        super(driver);
        waitForPageLoad(restaurantName);
    }

    /**
     * Adds a meal to the shopping basket
     * @param mealName meal to add
     */
    public void addMealToBasket(String mealName) {
        WebElement meal = getMealContainerByName(mealName);
        scrollBelowElement(meal, categoryNavigation);
        waitAndClick(meal);
        shoppingBasket = new ShoppingBasket(driver);
        waitForElementVisibility(shoppingBasket.getCartMealContainerByName(mealName));
    }

    /**
     * Selects a menu category
     * @param menuItem menu category to select
     */
    public void selectMenuItem(String menuItem) {
        WebElement menu = getMenuCategoryByName(menuItem);
        waitAndClick(menu);
        waitForElementVisibility(getMenuCardByName(menuItem));
    }

    private WebElement getMealContainerByName(String mealName) {
        String xpath = "//div[contains(@class,'meal-container') and .//span[@data-product-name='"
                + mealName + "']]";
        return getElementByXpath(xpath);
    }

    private WebElement getMenuCategoryByName(String menu) {
        String xpath = "//div[@class='menu-category-list']//a[contains(text(), '" + menu + "')]";
        return getElementByXpath(xpath);
    }

    private WebElement getMenuCardByName(String menu) {
        String xpath = "//div[contains(@class,'menucard__category-name') and normalize-space(text())='" + menu + "']";
        return getElementByXpath(xpath);
    }
}
