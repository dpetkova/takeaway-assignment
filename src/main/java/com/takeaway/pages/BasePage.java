/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.utils.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;

public class BasePage {

    private static final Integer ELEMENT_WAIT = Integer.valueOf(Config.get("wait.element"));
    private static final String NO_ELEMENT_MSG = "Element does not exist";

    protected WebDriverWrapper driver;

    public BasePage(WebDriverWrapper driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Finds the first WebElement from a list of WebElements containing the provided text
     * @param elementsList list of WebElement to search in
     * @param text text to search for
     * @return the first WebElement with the provided text
     * @throws NoSuchElementException when no element with the provided text has been found
     */
    public WebElement getElementFromListByText(List<WebElement> elementsList, String text) {
        return elementsList.stream()
                .filter(el -> el.getText().equals(text))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("There is no WebElement with text " + text));
    }

    /**
     * Finds the first WebElement using the given XPath expression
     * @param xpath the XPath expression to use
     * @return the first matching element on the current page
     * @throws NoSuchElementException when no element using the given XPath expression has been found
     */
    public WebElement getElementByXpath(String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        if (element != null) {
            return element;
        }
        throw new NoSuchElementException(NO_ELEMENT_MSG);
    }

    /**
     * Gets the visible (i.e. not hidden by CSS) text of the given element, including sub-elements
     * @param element the element
     * @return the visible text of this element
     *  @throws NoSuchElementException when the element is null
     */
    public String getText(WebElement element) {
        if (element != null) {
            return element.getText();
        }
        throw new NoSuchElementException(NO_ELEMENT_MSG);
    }

    /**
     * Gets a list of the visible (i.e. not hidden by CSS) text of the given elements, including sub-elements
     * @param elements list of elements
     * @return the visible text of the elements
     */
    public List<String> getText(List<WebElement> elements) {
        return elements.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    /**
     * Returns the value of input field
     * @param element the WebElement for which the value to be returned
     * @return the value of input field
     */
    public String getValueByJs(WebElement element) {
        return String.valueOf(driver.executeScript("return arguments[0].value", element));
    }

    /**
     * Waits for WebElement visibility and clicks on it
     * @param element the WebElement
     */
    public void waitAndClick(WebElement element) {
        waitForElementVisibility(element).click();
    }

    /**
     * Fills in information in an input, textarea or select
     * @param element form element (input, textarea or select) to be filled in
     * @param text text to be written or chosen
     * @throws InvalidArgumentException when the element's tag name ís not among: input, textarea, select
     */
    public void clearAndFill(WebElement element, String text) {
        waitForElementVisibility(element);

        String tag = element.getTagName();
        switch (tag) {
            case "input":
            case "textarea":
                element.clear();
                element.sendKeys(text);
                break;
            case "select":
                Select select = new Select(element);
                if (!text.equalsIgnoreCase(select.getFirstSelectedOption().getText())) {
                    select.selectByVisibleText(text);
                }
                break;
            default:
                throw new InvalidArgumentException("Incorrect WebElement tag");
        }
    }

    /**
     * Waits for an WebElement to become visible
     * @param element the WebElement
     * @return the WebElement
     */
    public WebElement waitForElementVisibility(WebElement element) {
        waitFor(ExpectedConditions.visibilityOf(element));
        return element;
    }

    /**
     * Waits for all WebElements to become visible
     * @param webElements list of WebElements
     * @return list of the WebElements
     */
    public List<WebElement> waitForElementsVisibility(List<WebElement> webElements) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_WAIT);
        wait.until(ExpectedConditions.visibilityOfAllElements(webElements));
        return webElements;
    }

    /**
     * Waits until a WebElement becomes invisible
     * @param element the WebElement that needs to disappear
     */
    public void waitForElementToDisappear(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_WAIT);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    /**
     * Waits for WebElement until condition is met
     * @param condition the expected condition
     * @param timeout the timeout in seconds when an expectation is called
     */
    private void waitFor(ExpectedCondition<WebElement> condition, Integer timeout) {
        timeout = timeout != null ? timeout : ELEMENT_WAIT;
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(condition);
    }

    /**
     * Waits for WebElement until condition is met
     * @param condition the expected condition
     */
    private void waitFor(ExpectedCondition<WebElement> condition) {
        waitFor(condition, ELEMENT_WAIT);
    }

    /**
     * Waits for WebElement to be visible for a page
     * @param element the WebElement
     */
    public void waitForPageLoad(WebElement element) {
        waitForElementVisibility(element);
    }

    /**
     * Scrolls to a particular set of coordinates in the document
     * @param element the element to scroll to
     * @param elementAbove element above the one to scroll to
     */
    public void scrollBelowElement(WebElement element, WebElement elementAbove) {
        int elementAboveHeight = elementAbove.getSize().getHeight();
        int elementY = element.getLocation().getY();

        driver.executeScript("window.scrollTo(0, " + (elementY - elementAboveHeight) + ")");
    }

    /**
     * Scrolls the page to the top of the element.
     * @param element WebElement to be scrolled to
     */
    public void scrollToElement(WebElement element) {
        driver.executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
