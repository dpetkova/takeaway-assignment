/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.driver;

import com.takeaway.utils.Config;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;

public class LocalChromeDriver extends ChromeDriver {

    private static final String HEADLESS = Config.get("headless.enabled");
    private static final String USER_AGENT = Config.get("headless.user-agent");
    private static final String CHROME_DRIVER = Config.get("webdriver.chrome.driver");

    public LocalChromeDriver() {
        super(configureChrome());
    }

    private static ChromeOptions configureChrome() {
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        HashMap<String, Object> pref = new HashMap<>();
        pref.put("safebrowsing.enabled", "false");

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", pref);

        if (isHeadless()) {
            options.addArguments("headless");
            options.addArguments("user-agent=" + USER_AGENT);
        }

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        options.merge(capabilities);

        return options;
    }

    private static boolean isHeadless() {
        return "true".equals(HEADLESS);
    }

}
