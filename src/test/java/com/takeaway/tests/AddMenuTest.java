/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.takeaway.tests;

import com.takeaway.pages.modules.SearchWidget;
import com.takeaway.pages.modules.ShoppingBasket;
import com.takeaway.pages.restaurant.RestaurantListPage;
import com.takeaway.utils.TestUtil;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Slf4j
public class AddMenuTest extends TestUtil {

    @BeforeMethod(description = "Load application on restaurant page and set location")
    public void loadPage() {
        driver.loadUrl(MAIN_URL);
        driver.maximizeWindow();

        SearchWidget searchWidget = new SearchWidget(driver);
        searchWidget.loadRestaurantsByArea("8888", "8888 Alpha");

        RestaurantListPage restaurantListPage = new RestaurantListPage(driver);
        restaurantListPage.selectRestaurantByName("Pan Pizza Man");
    }

    @Test(description = "Verify that you can not order on empty basket", priority = 2)
    public void orderEmptyBasket() {
        ShoppingBasket shoppingBasket = new ShoppingBasket(driver);
        shoppingBasket.orderMeal();

        boolean isButtonDisabled = shoppingBasket.isOrderButtonDisabled();
        boolean isButtonDisplayed = shoppingBasket.isOrderButtonDisplayed();

        log.info("Is order button disabled: " + isButtonDisabled);
        log.info("Is order button displayed: " + isButtonDisplayed);

        assertTrue(isButtonDisabled && isButtonDisplayed);
    }
}
