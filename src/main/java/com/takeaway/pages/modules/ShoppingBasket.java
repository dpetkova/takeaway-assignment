/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.modules;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingBasket extends BasePage {

    @FindBy(id = "ibasket")
    private WebElement basketContainer;

    @FindBy(xpath = "//button[contains(@class, 'cartbutton-button')]")
    private WebElement orderButton;

    public ShoppingBasket(WebDriverWrapper driver) {
        super(driver);
        waitForPageLoad(basketContainer);
    }

    /**
     * Returns the WebElement for a single meal in shopping cart by meal name
     * @param mealName meal name
     * @return the first matching element
     */
    public WebElement getCartMealContainerByName(String mealName) {
        String xpath = getCartMealXpathByName(mealName);
        return getElementByXpath(xpath);
    }

    /**
     * Deletes meal from shopping cart by name
     * @param mealName meal name
     */
    public void deleteMeal(String mealName) {
        WebElement deleteButton = getDeleteButtonByMeal(mealName);
        waitAndClick(deleteButton);
        waitForElementToDisappear(deleteButton);
    }

    /**
     * Orders meal from shopping cart and wait for next stage
     */
    public void orderMealAndProceed() {
        waitAndClick(orderButton);
        waitForElementToDisappear(orderButton);

    }

    /**
     * Orders meal from shopping cart
     */
    public void orderMeal() {
        waitAndClick(orderButton);
    }

    public boolean isOrderButtonDisabled() {
        String value = orderButton.getAttribute("class");
        return value != null && value.contains("btn-disabled");
    }

    public boolean isOrderButtonDisplayed() {
        return orderButton.isDisplayed();
    }

    private String getCartMealXpathByName(String mealName) {
        return "//div[@class='cart-single-meal' and .//span[contains(@class,'cart-meal-name') and text()='" + mealName + "']]";
    }

    private WebElement getDeleteButtonByMeal(String mealName) {
        return getCartMealContainerByName(mealName).findElement(By.xpath(".//button[@class='cart-meal-delete']"));
    }
}
