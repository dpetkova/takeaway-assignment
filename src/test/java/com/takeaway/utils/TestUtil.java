/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.utils;

import com.takeaway.driver.WebDriverFactory;
import com.takeaway.driver.wrapper.WebDriverWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
public class TestUtil {

    protected final static String MAIN_URL = Config.get("app.url");
    private final static String TEMP_DIR = Config.get("temp.dir");

    protected WebDriverWrapper driver;

    @BeforeMethod(description = "Setting up browser")
    public void startDriver() {
        driver = new WebDriverFactory().getDriver();
    }

    @AfterMethod(description = "Close and quit browser")
    public void recordFailure(ITestResult result) {
        String testMethodName = result.getName();
        if (ITestResult.FAILURE == result.getStatus()) {
            saveScreenshot(testMethodName);
            savePageSource(testMethodName);
        }

        driver.close();
        driver.quit();
    }

    private void saveScreenshot(String fileNameSuffix) {
        File screenshot = driver.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File(TEMP_DIR + DateTimeUtils.getNow("yyyyMMdd-HHmmss-")
                    + fileNameSuffix + ".png"));
            log.info("Test failed. Screenshot saved in " + TEMP_DIR);
        } catch (IOException e) {
            log.error("Error taking screenshot", e);
        }
    }

    private void savePageSource(String fileNameSuffix) {
        String pageSource = driver.getPageSource();
        try {
            FileUtils.writeStringToFile(new File(TEMP_DIR + DateTimeUtils.getNow("yyyyMMdd-HHmmss-")
                    + fileNameSuffix + ".html"), pageSource, StandardCharsets.UTF_8);
            log.info("Test failed. Rendered HTML for {} saved in {}", driver.getCurrentUrl(), TEMP_DIR);
        } catch (IOException e) {
            log.error("Error saving page source", e);
        }
    }
}
