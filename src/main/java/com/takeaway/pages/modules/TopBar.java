/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.takeaway.pages.modules;

import com.takeaway.driver.wrapper.WebDriverWrapper;
import com.takeaway.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TopBar extends BasePage {

    private final SearchWidget searchWidget = new SearchWidget(driver);

    @FindBy(xpath = "//div[@class='topbar-container']")
    private WebElement topBarContainer;

    @FindBy(xpath = "//div[@class='topbar__title-container']//button")
    private WebElement location;

    public TopBar(WebDriverWrapper driver) {
        super(driver);
        waitForPageLoad(topBarContainer);
    }

    /**
     * Returns the current location on restaurant page
     * @return the current location
     */
    public String getLocation() {
        return location.getText();
    }

    public void openLocationModal() {
        waitAndClick(location);
        waitForElementVisibility(searchWidget.locationSearchInput);
    }
}
