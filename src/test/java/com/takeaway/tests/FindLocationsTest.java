/*
 * Copyright 2021 Desislava Petkova
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.takeaway.tests;

import com.opencsv.exceptions.CsvException;
import com.takeaway.pages.modules.SearchWidget;
import com.takeaway.utils.CsvReader;
import com.takeaway.utils.TestUtil;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Slf4j
public class FindLocationsTest extends TestUtil {

    @DataProvider(name = "incorrect-delivery-area")
    public static Object[][] dataProviderNameFromFile() throws IOException, CsvException {
        return CsvReader.readCsvFile("incorrect-delivery-area.csv");
    }

    @BeforeMethod(description = "Load application")
    public void loadPage() {
        driver.loadUrl(MAIN_URL);
        driver.maximizeWindow();
    }

    @Test(description = "Verify list of available delivery areas", priority = 2)
    public void verifyAvailableAreas() {
        List<String> expectedAreas = Arrays.asList("8888 Alpha", "8888 Beta");

        SearchWidget searchWidget = new SearchWidget(driver);
        searchWidget.typeLocation("8888").
                show(true);
        assertThat("Available locations do not match ", searchWidget.getAllAvailableDeliveryAreas(), equalTo(expectedAreas));
    }

    @Test(description = "Verify error message on incorrect delivery area", priority = 3, dataProvider = "incorrect-delivery-area")
    public void verifyErrorMessageOnDeliveryArea(String area, String expectedMessage) {
        SearchWidget searchWidget = new SearchWidget(driver);
        searchWidget.loadAreasWithError(area);
        log.info("Text in input search field: " + searchWidget.getSearchedAreaText());

        assertThat("Incorrect error message", searchWidget.getDeliveryErrorText(), equalTo(expectedMessage));
    }
}
